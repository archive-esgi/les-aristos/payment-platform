const express = require('express');
const twig = require('twig');
const session = require('express-session');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

/* db connection */
global.db = require('./models/');

app.set('views', __dirname + '/views');
app.set('view engine', 'twig');
app.set('twig options', {
    strict_variables: false
});

app.use(session({
    secret: 'karl le bg'
}));

app.use('/static', express.static('public'));

app.use(bodyParser.urlencoded({ extended: false }))

app.get("/", (req, res) => {
    res.render('index', {
        items: [
            {
                title: 'Ugly Theme',
                description: 'The most ugly theme I ever seen !!',
                price: 120
            },
            {
                title: 'Some Talent',
                description: "It's for a friend ?",
                price: 100
            },
            {
                title: 'Vetixy Premium Subscription',
                description: 'Too keep safe yours animals',
                price: 14
            }
        ],
        price: 234,
        currency: 'EUR',
        currencySymbol: '€'
    });
});

require('./lib/loadSettings').load();

require('./routes/admin')(app);
require('./routes/checkout')(app);

app.listen(port, () => {
    console.log(`Started at http://localhost: ${port}`);
});