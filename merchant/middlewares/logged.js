const logged = (req, res, next) => {
    if (!req.session.auth) {
        return res.redirect('/admin/login');
    } else {
        return next();
    }
};

module.exports = logged;