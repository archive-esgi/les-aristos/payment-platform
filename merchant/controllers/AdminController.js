'use strict';

const axios = require('axios');
const db = require("../models");

exports.login = (req, res) => {
    if (req.session.auth) {
        return res.redirect('/admin');
    }

    return res.render('admin/login', {
        error: req.query.e ? true : false
    });
}

exports.loginCheck = (req, res) => {
    let urlError = '/admin/login?e=1';

    if (!req.body.email || !req.body.password) {
        return res.redirect(urlError);
    }

    if (req.body.email === process.env.ADMIN_EMAIL && req.body.password === process.env.ADMIN_PASSWORD) {
        req.session.auth = true;
        return res.redirect('/admin');
    }

    return res.redirect(urlError);
}

exports.logout = (req, res) => {
    req.session.destroy((err) => {});
    return res.redirect('/admin/login');
}

exports.index = async (req, res) => {
    const totalOrderCount = await db.Order.count()
        .then(count => count)
        .catch(() => 0);

    const confirmedOrderCount = await db.Order.count({
            where: {
                status: 'confirmed'
            }
        })
        .then(count => count)
        .catch(() => 0);

    const canceledOrderCount = await db.Order.count({
            where: {
                status: 'canceled'
            }
        })
        .then(count => count)
        .catch(() => 0);
    
    const lastMonth = new Date(new Date().setDate(new Date().getDate() - 30));
    const orderInLastMonthCount = await db.Order.count({
            where: {
                $gte: lastMonth
            }
        })
        .then(count => count)
        .catch(() => 0);

    res.render('admin/index', {
        totalOrderCount,
        confirmedOrderCount,
        canceledOrderCount,
        orderInLastMonthCount
    });
}

exports.getOrders = async (req, res) => {
    const orders = await db.Order.findAll()
        .then(orders => orders)
        .catch(() => []);
    res.render('admin/orders', {
        orders
    });
}

exports.getSettings = async (req, res) => {
    const settings = await db.Setting.findAll({
        order: [
            ['id', 'DESC']
        ],
    })
        .then(settings => settings)
        .catch(() => []);
    res.render('admin/settings', {
        settings
    });
}

exports.getSettingsUpdate = (req, res) => {
    Object.keys(req.body).forEach(async key => {
        await db.Setting.update({ value: req.body[key] }, {
            where: {key}
        })
        .then(([nbUpdate, setting]) => {
            return setting;
        })
        .catch(err => {
            console.log(err);
        });
    });

    return res.redirect('/admin/settings');
}

exports.refund = async (req, res) => {
    if (!req.body.transaction) {
        return res.status(400).json({
            message: "transaction parameter not finded"
        }).end();
    }

    let client_secret = await db.Setting.findOne({
        where: {
            key: 'client_secret'
        }
    })
    .then(setting => setting)
    .catch((err) => {
        console.log(err);
        return null;
    });

    if (!client_secret) {
        return res.status(500).end();
    }

    let data = {
        transactionToken: req.body.transaction,
        client_secret: client_secret.value
    };

    let request = await axios.post('http://platform:3000/api/transactions/refund', data)
        .then(async response => {
            if (response.status !== 200) {
                if (error.response.data && error.response.data.message) {
                    return res.status(500).json(error.response.data.message).end();
                } else {
                    return res.status(504).end();
                }
            }

            let order = await db.Order.findOne({
                where: {
                    transaction: req.body.transaction
                }
            })
            .then(order => order)
            .catch((err) => {
                console.log(err);
                return null;
            });

            if (!order) {
                return res.status(400).json({
                    message: "Order not finded !"
                }).end();
            }
        
            if (order.status !== "confirmed") {
                return res.status(400).json({
                    message: "Order not confirmed !"
                }).end();
            }

            order.status = "refunded";
            await order.save();

            return res.status(200).end();
        })
        .catch(error => {
            console.log(error);

            if (error.response.data && error.response.data.message) {
                return res.status(500).json(error.response.data.message).end();
            } else {
                return res.status(504).end();
            }
        });
    return request;
}