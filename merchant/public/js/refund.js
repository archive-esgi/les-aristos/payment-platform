
var refundButtons = document.getElementsByClassName("refundButton");

for(var i = 0; i < refundButtons.length; i++) {
    (index => {
        refundButtons[index].addEventListener("click", async event => {
            event.preventDefault();
        
            var button = event.target;
            const transaction = button.dataset.transaction;
        
            if (confirm("Did you really want refund this order ?")) {
                button.disabled = true;
                await fetch('/admin/orders/refund', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        transaction
                    })
                })
                .then(response => {
                    response.text().then(function(text) {
                        let data = text ? JSON.parse(text) : {};

                        if (response.status === 200) {
                            alert("Order refunded !");
                            button.remove();
                            document.location.reload(true);
                        } else {
                            console.log(response);
                            alert(data.message ?? "An error was occured !");
                            button.disabled = false;
                        }
                    });
                })
                .catch(error => {
                    console.log(error);
                });
            }
        });
    })(i);
}