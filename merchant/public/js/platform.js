document.getElementById("goToPayment").addEventListener("click", async event => {
    event.preventDefault();
    let firstname = document.getElementById("firstName").value;
    let lastname = document.getElementById("lastName").value;
    let email = document.getElementById("email").value;
    
    if (!email || !lastname || !firstname) {
        return alert('Please fill all informations');
    }

    let user = {
        firstname: document.getElementById("firstName").value,
        lastname: document.getElementById("lastName").value,
        email: document.getElementById("email").value
    };

    await fetch('/checkout/transaction', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            price,
            currency,
            user
        })
    })
    .then(response => {
        if (response.status === 201) {
            response.json().then(data => {
                document.location.href = data.url;
            });
            return;
        }

        alert('Something was wrong ... Please try again later.');
    })
    .catch(error => {
        console.log(error);
    });
});