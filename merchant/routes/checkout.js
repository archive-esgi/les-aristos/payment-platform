module.exports = app => {
    const checkoutController = require('../controllers/CheckoutController');
    const router = require('express').Router();
    const bodyParser = require('body-parser');
    const jsonParser = bodyParser.json();

    router.post("/transaction", jsonParser, checkoutController.transaction);
    router.get("/confirmation", checkoutController.confirmation);
    router.get("/cancel", checkoutController.cancel);

    app.use('/checkout', router);
}
