'use strict';

exports.load = () => {
    const settings = [
        'client_token',
        'client_secret'
    ];

    setTimeout(() => {
        settings.forEach(async setting => {
            let result = await db.Setting.findOne({
                where: {
                    key: setting
                }
            })
            .then(settingFind => settingFind)
            .catch((err) => {
                console.log(err);
                return null;
            });
    
            if (!result) {
                await db.Setting.create({
                    key: setting,
                    value: ""
                }).then(settingCreated => {
                    console.log(`Load ${setting} setting !`);
                })
                .catch((err) => {
                    console.log(err);
                });
            }
        });
    }, 30000);
};