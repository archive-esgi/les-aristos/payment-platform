import React from 'react';
import {useMemo} from 'react';

const TextField = ({className, type, placeholder, id, name, onChange, value, defaultValue, disabled, error}) => {
    return useMemo(() =>
        <>
            <input
                className={className}
                type={type}
                placeholder={placeholder}
                id={id}
                name={name}
                onChange={onChange}
                value={value}
                defaultValue={defaultValue}
                disabled={disabled}
            />
            <p className={`${error ? 'text-red-500 text-xs italic my-1' : ''}`}>
                {error}
            </p>
        </>,
        [
            className,
            type,
            placeholder,
            id,
            name,
            onChange,
            value,
            defaultValue,
            disabled,
            error
        ]
    );
}

export default TextField;