import React from 'react';
import {useMemo} from 'react';

const Button = ({className, type, onSubmit, onClick, disabled, children}) => {
    return useMemo(() =>
        <>
            <button
                className={className}
                type={type}
                onSubmit={onSubmit}
                onClick={onClick}
                disabled={disabled}
            >
                {children}
            </button>
        </>,
        [
            className,
            type,
            onSubmit,
            onClick,
            disabled,
            children
        ]
    );
}

export default Button;