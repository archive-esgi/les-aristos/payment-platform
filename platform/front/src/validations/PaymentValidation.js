import {
    validateCreditCard,
    validateCryptoCard,
    validateMonth,
    validateYear,
    validateNotEmpty
} from './index';

export const validate = values => {
    let errors = {};

    errors.cardNumber = validateCreditCard(values.cardNumber);
    errors.cardCrypt = validateCryptoCard(values.cardCrypt);
    errors.cardMonthEnd = validateMonth(values.cardMonthEnd);
    errors.cardYearEnd = validateYear(values.cardYearEnd);
    errors.cardOwner = validateNotEmpty(values.cardOwner);

    return errors;
}