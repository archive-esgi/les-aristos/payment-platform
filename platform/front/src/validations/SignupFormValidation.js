import {validateNotEmpty,
    validateEmail,
    validateSiret,
    validatePasswordEqual,
    validateCurrency,
    validatePdfFile
} from './index';

export const validate = values => {
    let errors = {};

    errors.social_reason = validateNotEmpty(values.social_reason);
    errors.siret = validateSiret(values.siret);
    errors.email = validateEmail(values.email);
    errors.phone = validateNotEmpty(values.phone);
    errors.currency = validateNotEmpty(values.currency);
    errors.kbis = validatePdfFile(values.kbis);
    errors.url_confirmation = validateNotEmpty(values.url_confirmation);
    errors.url_cancel = validateNotEmpty(values.url_cancel);
    errors.password = validateNotEmpty(values.password);
    errors.password_confirm = validatePasswordEqual(values.password, values.password_confirm);

    return errors;
}