export const validateNotEmpty = value =>
    value
        ? undefined
        : 'Le champ est requis';

export const validateEmail = value => 
    !/\S+@\S+\.\S+/.test(value)
        ? 'Le format de l\'adresse e-mail est incorrect'
        : undefined;

export const validateSiret = value => {
    if (value !== undefined) {
        if (value.length !== 14) {
            return 'Le SIRET doit contenir 14 chiffres';
        }
        if (!/^\d+$/.test(value)) {
            return 'Le SIRET doit contenir que des chiffres';
        }
        return;
    }
    return 'Le champs est requis';
}

export const validateCreditCard = value => {
    if (value !== undefined) {
        if (!/^\d+$/.test(value)) {
            return 'Le format est incorrecte';
        }
        return;
    }
    return 'Le champs est requis';
}

export const validateCryptoCard = value => {
    if (value !== undefined) {
        if (!/^[1-9]\d{0,2}(?:\,\d{1,3})?$/.test(value)) {
            return 'Le cryptogramme doit être composé de 3 chiffres';
        }
        return;
    }
    return 'Le champs est requis';
}

export const validateCurrency = value => {
    if (value !== undefined) {
        if (!/^[A-Z]{0,3}(?:\,[A-Z]{1,4})?$/.test(value)) {
            return 'Vous n\'avez pas sélectionné de devise';
        }
        return;
    }
    return 'Le champs est requis';
}

export const validateMonth = value => {
    if (value !== undefined) {
        if (!/^(0?[1-9]|1[012])$/.test(value)) {
            return 'Mois incorrecte';
        }
        return;
    }
    return 'Le champs est requis';
}

export const validateYear = value => {
    if (value !== undefined) {
        if (!/^[1-9]\d{0,3}(?:\,\d{1,4})?$/.test(value)) {
            return 'Année incorrecte';
        }
        return;
    }
    return 'Le champs est requis';
}

export const validatePasswordEqual = (value1, value2) => 
    (value1 === value2 && value2 === value1)
        ? undefined
        : 'Confirmation incorrecte';

export const validatePdfFile = (value) => {
    const input = document.querySelector('input[name="kbis"]')
    if (input !== undefined) {
        if (!input.value.includes('.pdf')) {
            return 'Le fichier n\'est pas au format .pdf'
        }
        return;
    }
    return 'Un fichier au format .pdf est requis';
    
}