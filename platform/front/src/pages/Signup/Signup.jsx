import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';
import useForm from '../../hooks/useForm';
import useAuth from '../../hooks/useAuth';
import useMerchant from '../../hooks/useMerchant';
import useCurrency from '../../hooks/useCurrency';
import {validate} from '../../validations/SignupFormValidation';
import TextField from '../../components/TextField/TextField';
import Button from '../../components/Button/Button';
import {Redirect} from 'react-router-dom';
import Alert from '../../components/Alert/Alert';

const Signup = () => {
    const {actions, selectors} = useAuth();
    const {merchantSelectors, merchantActions} = useMerchant();
    const {currencyActions, currencySelectors} = useCurrency();
    const {
        handleChange,
        handleSubmit,
        inputFormValues,
        errors
    } = useForm(onSignup, validate);
    const currencies = currencySelectors.getCurrencies();
    const kbisId = merchantSelectors.getKbisId();

    useEffect(() => {
        // document.querySelector("input[name='iban']").addEventListener('input', e => {
        //     e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
        // });

        currencyActions.fetchCurrencies();
    }, []);

    function onSignup() {
        const formattedInput = ['social_reason', 'siret', 'email', 'phone', 'url_confirmation', 'url_cancel'];
        formattedInput.forEach(element => inputFormValues[element] = inputFormValues[element].trim());

        const kbisValue = {
            kbis: kbisId
        }

        actions.signup({
            ...inputFormValues,
            ...kbisValue
        });
    }

    function onKbisUpload(e) {
        const files = e.target.files;
        const formData = new FormData();
        formData.append('kbis', files[0]);
        merchantActions.sendKbisToReceiveId(formData);
    }

    if (selectors.isLogged()) {
        return <Redirect to="/dashboard" />;
    }

    return (
        <React.Fragment>
            <div className="flex justify-center m-12">
                <form onSubmit={handleSubmit} className="w-full max-w-lg rounded shadow-lg p-8">
                <p className="text-2xl mb-3">Créez votre compte</p>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full px-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="email">
                                E-mail
                            </label>
                            <TextField
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                type="text"
                                placeholder="begajik864@mailres.net"
                                name="email"
                                onChange={handleChange}
                                value={inputFormValues.email || ''}
                                error={errors.email}
                            />
                        </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="password">
                                Mot de passe
                            </label>
                            <TextField
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                type="password"
                                placeholder="*************"
                                name="password"
                                onChange={handleChange}
                                value={inputFormValues.password || ''}
                                error={errors.password}
                            />
                        </div>
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="password_confirm">
                                Confirmation
                            </label>
                            <TextField
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                type="password"
                                placeholder="*************"
                                name="password_confirm"
                                onChange={handleChange}
                                value={inputFormValues.password_confirm || ''}
                                error={errors.password_confirm}
                            />
                        </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="social_reason">
                                Raison sociale
                            </label>
                            <TextField
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                type="text"
                                placeholder="Raison sociale"
                                name="social_reason"
                                onChange={handleChange}
                                value={inputFormValues.social_reason || ''}
                                error={errors.social_reason}
                            />
                        </div>
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="siret">
                                SIRET
                            </label>
                            <TextField
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                type="text"
                                placeholder="XXX XXX XXX XXXXX"
                                name="siret"
                                onChange={handleChange}
                                value={inputFormValues.siret || ''}
                                error={errors.siret}
                            />
                        </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="phone">
                                Téléphone
                            </label>
                            <TextField
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" 
                                type="text"
                                placeholder="Téléphone"
                                name="phone"
                                onChange={handleChange}
                                value={inputFormValues.phone || ''}
                                error={errors.phone}
                            />
                        </div>
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="currency">
                            DEVISE DE REVERSEMENT
                        </label>
                            <div className="relative">
                                <select
                                    className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="currency"
                                    name="currency"
                                    onChange={handleChange}
                                    value={inputFormValues.currency || ''}
                                >
                                    <option>Sélectionner : </option>
                                    {currencies.map((currency, i) => <option key={i}>{currency.iso}</option>)}
                                </select>
                                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                </div>
                            </div>
                            <span className="text-red-500 text-xs italic">{errors.currency}</span>
                        </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full px-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="iban">
                                KBIS
                            </label>
                            <TextField
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                type="file"
                                name="kbis"
                                onChange={onKbisUpload}
                                error={errors.kbis}
                            />
                            {merchantSelectors.getKbisUploadError() &&
                                <Alert
                                    color="red"
                                    role="alert"
                                    text={merchantSelectors.getKbisUploadError()}
                                />
                            }
                        </div>
                        
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full px-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="iban">
                                IBAN
                            </label>
                            <TextField
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                type="text"
                                placeholder="FRXX XXXX XXXX XXXX XXXX XXXX XXX"
                                name="iban"
                                onChange={handleChange}
                                value={inputFormValues.iban || ''}
                                error={errors.iban}
                            />
                        </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="url_confirmation">
                                URL de confirmation 
                            </label>
                            <TextField
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" 
                                type="text"
                                name="url_confirmation"
                                onChange={handleChange}
                                value={inputFormValues.url_confirmation || ''}
                                error={errors.url_confirmation}
                            />
                        </div>
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="url_cancel">
                                URL de redirection 
                            </label>
                            <TextField
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" 
                                type="text"
                                name="url_cancel"
                                onChange={handleChange}
                                value={inputFormValues.url_cancel || ''}
                                error={errors.url_cancel}
                            />
                        </div>
                    </div>
                    
                    <div className="flex text-center">
                        <Button
                            type="submit"
                            className="rounded-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-full px-3 mb-6 md:mb-0"
                        >
                            Continuer
                        </Button>
                    </div>
                    <div className="text-center mt-2">
                        <p className="text-base">Vous avez déjà un compte ? {' '}
                            <Link className="underline text-blue-600" to="/signin">Je me connecte</Link>
                        </p>
                    </div>
                </form>
            </div>
        </React.Fragment>
    );
}

export default Signup;