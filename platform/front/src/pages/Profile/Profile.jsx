import React, {useEffect} from 'react';
import TextField from '../../components/TextField/TextField';
import Button from '../../components/Button/Button';
import Moment from 'moment';
import useAuth from '../../hooks/useAuth';
import useUser from '../../hooks/useUser';
import useForm from '../../hooks/useForm';
import useMerchant from '../../hooks/useMerchant';
import {Link, useHistory} from 'react-router-dom';
import {validate} from '../../validations/ProfileFormValidation';
import Alert from '../../components/Alert/Alert';

const Profile = () => {
    const history = useHistory();
    const token = localStorage.getItem('token');
    const merchantId = localStorage.getItem('merchantId')

    const {actions, selectors} = useAuth();
    const {userActions, userSelectors} = useUser();
    const {merchantActions, merchantSelectors} = useMerchant();

    const userId = selectors.userInfo().id;
    const user = userSelectors.getUser();
    const merchant = merchantSelectors.getMerchant();

    const {
        handleChange,
        handleSubmit,
        inputFormValues,
        errors
    } = useForm(onUpdate, validate);
    
    useEffect(() => {
        userActions.fetchUser(token, userId);
        merchantActions.fetchMerchant(token, merchantId);
    }, []);

    function onUpdate() {
        userActions.updateUser(token, userId, {...inputFormValues});
    }

    return (
        <React.Fragment>
            <div className="ml-64 bg-gray-300">
                <div className="max-w-full w-full lg:max-w-full p-8 h-screen">
                        <div className="mb-4">
                        <div
                            onClick={() => history.goBack()}
                            className="cursor-pointer inline-flex items-center bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow"
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>
                            <span className="ml-2">Retour</span>
                        </div>
                    </div>
                    <div className="border border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal shadow-lg">
                        <p className="text-gray-900 uppercase font-bold text-xl mb-6 border-b">Profil</p>
                        <form onSubmit={handleSubmit}>
                            <p className="text-gray-700 uppercase font-bold text-md mb-4">Informations sur le compte utilisateur</p>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        E-mail
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="email"
                                        defaultValue={user && user.email}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                                <div className="w-full px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Date de création
                                    </label>
                                    <input className="opacity-50 cursor-not-allowed appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="createdAt"
                                        defaultValue={user && Moment(user.createdAt).format('DD-MM-YYYY')}
                                        disabled
                                        onChange={handleChange}
                                    />
                                </div>
                                <div className="w-full px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Dernière mise à jour
                                    </label>
                                    <input
                                        className="opacity-50 cursor-not-allowed appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="updatedAt"
                                        defaultValue={user && Moment(user.updatedAt).format('DD-MM-YYYY à h:mm:ss')}
                                        disabled
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                                <div className="w-full px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Client token
                                    </label>
                                    <input className="opacity-50 cursor-not-allowed appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="client_token"
                                        defaultValue={merchant && merchant.client_token}
                                        disabled
                                        onChange={handleChange}
                                    />
                                </div>
                                <div className="w-full px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Client secret
                                    </label>
                                    <input
                                        className="opacity-50 cursor-not-allowed appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="client_secret"
                                        defaultValue={merchant && merchant.client_secret}
                                        disabled
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                            <div className="mb-6">
                                {userSelectors.getSuccessUpdateUserMsg() &&
                                    <Alert
                                        color="green"
                                        role="alert"
                                        text={userSelectors.getSuccessUpdateUserMsg()}
                                    />
                                }
                            </div>
                            <div className="mt-2">
                                <button
                                    type="submit"
                                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-6 rounded-full"
                                >
                                    Enregistrer
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                {/* <div className="max-w-full w-full lg:max-w-full p-8 h-screen">
                    <div className="border border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal shadow-lg">
                        <p className="text-gray-900 font-bold text-xl border-b">Clés API</p>
                        <table className="table-fixed">
                            <thead>
                                <tr className="bg-gray-100">
                                    <th className="border px-4 py-2 w-1/4">Nom</th>
                                    <th className="border px-4 py-2 w-1/2">Token</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="border px-4 py-2">Clé publique</td>
                                    <td className="border px-4 py-2">pk_live_51DxMxTG2veA4GcMJXSyNmx3rDbhrN35RN2E5uA2UQ1nYfKOMcb3sX9hqj8AFzRULF5AtuYjJAzOs63I1ZAOW6MRY00b38xeN32</td>
                                </tr>
                                <tr>
                                    <td className="border px-4 py-2">Clé privée</td>
                                    <td className="border px-4 py-2">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</td>
                                </tr>
                            </tbody>
                            </table>
                    </div>
                </div> */}
            </div>
        </React.Fragment>
    )
}

export default Profile;