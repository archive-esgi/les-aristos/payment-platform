import React, {useEffect} from 'react';
import TextField from '../../components/TextField/TextField';
import useForm from '../../hooks/useForm';
import Button from '../../components/Button/Button';
import { useLocation, useHistory, Link, Redirect } from 'react-router-dom';
import useTransaction from '../../hooks/useTransaction';
import {validate} from '../../validations/PaymentValidation';
import Alert from '../../components/Alert/Alert';

const Payment = () => {
    const location = useLocation();
    const history = useHistory();
    const {
        handleChange,
        handleSubmit,
        errors,
        inputFormValues
    } = useForm(onPayment, validate);
    const {transactionSelectors, transactionActions} = useTransaction();
    const transacUrlToken = new URLSearchParams(location.search);
    const transacUrlTokenValue = transacUrlToken.get('t');
    const transaction = transactionSelectors.getTransactionByToken();

    useEffect( () => {
        // document.querySelector("input[name='cardNumber']").addEventListener('input', e => {
        //     e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
        // });
        transactionActions.fetchTransactionByToken(transacUrlTokenValue);
    }, []);

    const paymentValues = {
        ...inputFormValues,
        transactionToken: transacUrlTokenValue
    }

    function onPayment() {
        transactionActions.paymentTransaction({...paymentValues});
    }

    function onCancel() {
        transactionActions.cancelTransaction({transactionToken: transacUrlTokenValue});
    }
    
    return (
        <React.Fragment>
            <div className=" h-screen bg-gray-300 pt-12">
                <div className="flex justify-center container mx-auto items-start">
                    {transaction && transaction.status !== 'created' ?
                        <>
                            <div className="rounded shadow-lg p-8 bg-white lg:1/2">
                                <p>La page que vous recherchez est actuellement indisponible !</p>
                                <Button
                                    onClick={() => history.goBack()}
                                    className="mt-4 cursor-pointer inline-flex items-center bg-white hover:bg-gray-100 text-gray-800 font-semibold py-1 px-3 border border-gray-400 rounded shadow"
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>
                                    <span className="ml-2">Retour</span>
                                </Button>
                            </div>
                        </>
                        :
                        <>
                            <div className="rounded shadow-lg p-8 bg-white w-2/4 mr-4">
                                <p className="text-3xl mb-3">Informations de paiment</p>
                                <form onSubmit={handleSubmit}>
                                    <div className="flex flex-wrap  px-3 -mx-3 mb-6">
                                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                                Numéro de carte
                                            </label>
                                            <TextField
                                                className={`${errors.cardNumber ? 'border-red-500' : ''} appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500`}
                                                type="text"
                                                name="cardNumber"
                                                placeholder="1234 1234 1234 1234"
                                                onChange={handleChange}
                                                value={inputFormValues.cardNumber || ''}
                                                error={errors.cardNumber}
                                            />
                                    </div>
                                    <div className="flex flex-wrap -mx-3 mb-6">
                                        <div className="w-full md:w-1/2 px-3">
                                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                                Mois d'expiration
                                            </label>
                                            <TextField
                                                className={`${errors.cardMonthEnd ? 'border-red-500' : ''} appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500`}
                                                type="text"
                                                name="cardMonthEnd"
                                                placeholder="MM"
                                                onChange={handleChange}
                                                value={inputFormValues.cardMonthEnd || ''}
                                                error={errors.cardMonthEnd}
                                            />
                                        </div>
                                        <div className="w-full md:w-1/2 px-3">
                                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                                Année d'expiration
                                            </label>
                                            <TextField
                                                className={`${errors.cardYearEnd ? 'border-red-500' : ''} appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500`}
                                                type="text"
                                                name="cardYearEnd"
                                                placeholder="AAAA"
                                                onChange={handleChange}
                                                value={inputFormValues.cardYearEnd || ''}
                                                error={errors.cardYearEnd}
                                            />
                                        </div>
                                    </div>
                                    <div className="flex flex-wrap -mx-3 mb-6">
                                        <div className="w-full md:w-1/2 px-3">
                                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                                Nom complet
                                            </label>
                                            <TextField
                                                className={`${errors.cardOwner ? 'border-red-500' : ''} appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500`}
                                                type="text"
                                                name="cardOwner"
                                                placeholder="Jean Dupont"
                                                onChange={handleChange}
                                                value={inputFormValues.cardOwner || ''}
                                                error={errors.cardOwner}
                                            />
                                        </div>
                                        <div className="w-full md:w-1/2 px-3">
                                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                                Cryptogramme visuel
                                            </label>
                                            <TextField
                                                className={`${errors.cardCrypt ? 'border-red-500' : ''} appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500`}
                                                type="text"
                                                name="cardCrypt"
                                                placeholder="CVV"
                                                onChange={handleChange}
                                                value={inputFormValues.cardCrypt || ''}
                                                error={errors.cardCrypt}
                                            />
                                        </div>
                                        {transactionSelectors.errorMsg() &&
                                            <Alert
                                                color="red"
                                                className="w-full"
                                                role="alert"
                                                text={transactionSelectors.errorMsg()}
                                            />
                                        }
                                    </div>
                                    <div>
                                        <Button
                                            className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mr-4"
                                            onClick={onCancel}
                                        >
                                            Annuler
                                        </Button>
                                        <Button
                                            type="submit"
                                            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                        >
                                            Payer
                                        </Button>
                                    </div>
                                </form>
                            </div>
                            <div className="rounded shadow-lg bg-white p-8 w-1/4">
                                <p className="text-lg font-bold">Montant Total : </p>
                                <p className="text-lg font-bold">{transaction && transaction.price} {transaction && transaction.currency}</p>
                            </div>
                        </>
                    }
                </div>
            </div>
        </React.Fragment>
    );
}

export default Payment;