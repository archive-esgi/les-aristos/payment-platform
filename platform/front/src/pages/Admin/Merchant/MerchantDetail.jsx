import React, {useEffect} from 'react';
import Moment from 'moment';
import useMerchant from '../../../hooks/useMerchant';
import useForm from '../../../hooks/useForm';
import {useParams} from 'react-router-dom';
import {validate} from '../../../validations/UpdateMerchantValidation';
import TextField from '../../../components/TextField/TextField';
import Button from '../../../components/Button/Button';
import {Link, useHistory} from 'react-router-dom';
import Alert from '../../../components/Alert/Alert';

const MerchantDetail = () => {
    const history = useHistory();
    const token = localStorage.getItem('token');
    const {merchantSelectors, merchantActions} = useMerchant();
    const {id} = useParams();
    const {
        handleChange,
        handleSubmit,
        errors,
        inputFormValues
    } = useForm(onUpdate, validate);
    const merchant = merchantSelectors.getMerchant();

    useEffect(() => {
        merchantActions.fetchMerchant(token, id);
    }, []);

    function onUpdate() {
        merchantActions.updateMerchant(token, id, {...inputFormValues});
    }

    function onDelete() {
        if (window.confirm('Etes vous sur de vouloir supprimer ce marchand ?')) {
            merchantActions.deleteMerchant(token, id);
        }
    }

    return (
        <React.Fragment>
            <div className="ml-64 bg-gray-300">
                <div className="max-w-full w-full lg:max-w-full p-8 h-screen">
                    <div className="mb-4">
                        <div
                            onClick={() => history.goBack()}
                            className="cursor-pointer inline-flex items-center bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow"
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>
                           <span className="ml-2">Retour</span>
                        </div>
                    </div>
                    
                    <div className="bg-white rounded-lg p-4 flex flex-col justify-between leading-normal shadow-lg mb-10">
                        <div className="flex justify-between mb-6">
                            <p className="text-gray-900 uppercase font-bold text-xl">Détail</p>
                            {/* <Button 
                                className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-1 rounded"
                                onClick={onDelete}
                            >
                                <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                                    <path d="M0 0h24v24H0z" fill="none"/>
                                    <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" fill="white" />
                                </svg>
                            </Button> */}
                        </div>
                        <form onSubmit={handleSubmit}>
                            <p className="text-gray-700 uppercase font-bold text-md mb-4">Informations sur le marchand</p>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Raison sociale
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="social_reason"
                                        defaultValue={merchant && merchant.social_reason}
                                        onChange={handleChange}
                                    />
                                    <p className="text-red-500 text-xs italic mb-3">{errors.social_reason}</p>
                                </div>
                            </div>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        SIRET
                                    </label>
                                    <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="siret"
                                        defaultValue={merchant && merchant.siret}
                                        onChange={handleChange}

                                    />
                                </div>
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Phone
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="phone"
                                        defaultValue={merchant && merchant.phone}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        IBAN
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="iban"
                                        defaultValue={merchant && merchant.iban}
                                        onChange={handleChange}
                                    />
                                </div>
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        KBIS
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="kbis"
                                        defaultValue={merchant && merchant.kbis}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Token
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="client_token"
                                        defaultValue={merchant && merchant.client_token}
                                        onChange={handleChange}
                                    />
                                </div>
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Clé secrète
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="client_secret"
                                        defaultValue={merchant && merchant.client_secret}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                                <div className="w-full px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Url de confirmation
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="url_confirmation"
                                        defaultValue={merchant && merchant.url_confirmation}
                                        onChange={handleChange}
                                    />
                                </div>
                                <div className="w-full px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        URL d'annulation
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="url_cancel"
                                        defaultValue={merchant && merchant.url_cancel}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                            <div className="mb-6">
                                {merchantSelectors.getSuccessUpdateMsg() &&
                                    <Alert
                                        color="green"
                                        role="alert"
                                        text={merchantSelectors.getSuccessUpdateMsg()}
                                    />
                                }
                            </div>
                            <div className="mt-2">
                                <Button
                                    type="submit"
                                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-6 rounded-full"
                                >
                                    Enregistrer
                                </Button>
                            </div>
                        </form>
                        <React.Fragment>
                            <p className="text-gray-700 uppercase font-bold text-md mb-4 mt-6">Informations sur le compte associé</p>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        E-mail
                                    </label>
                                    <input
                                        className="opacity-50 cursor-not-allowed appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="email"
                                        disabled
                                        defaultValue={merchant && merchant.User.email}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Date de création
                                    </label>
                                    <input className="opacity-50 cursor-not-allowed appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="createdAt"
                                        disabled
                                        defaultValue={merchant && Moment(merchant.User.createdAt).format('DD-MM-YYYY')}
                                        onChange={handleChange}
                                    />
                                </div>
                                <div className="w-full px-3 mb-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Dernière mise à jour
                                    </label>
                                    <input
                                        className="opacity-50 cursor-not-allowed appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        type="text"
                                        name="updatedAt"
                                        disabled
                                        defaultValue={merchant && Moment(merchant.User.updatedAt).format('DD-MM-YYYY à h:mm:ss')}
                                        onChange={handleChange}
                                    />
                                </div>
                            </div>
                            <div className="flex flex-wrap -mx-3 lg:flex-no-wrap xl:w-5/6 md:mb-0">
                            <div className="w-full px-3 mb-3">
                                <label className="uppercase tracking-wide text-gray-700 text-xs font-bold mb-2 mr-2">
                                    Etat
                                </label>
                                <span className={`${merchant && merchant.User.active ? 'bg-green-400' : 'bg-red-400'} rounded py-1 px-3 text-xs text-white font-bold`}>
                                    {merchant && merchant.User.active ? 'Activé' : 'Désactivé'}
                                </span>
                                </div>
                            </div>
                            {/* <div className="mt-2">
                                <button
                                    type="submit"
                                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-6 rounded-full"
                                >
                                    Modifier
                                </button>
                            </div> */}
                        </React.Fragment>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default MerchantDetail;