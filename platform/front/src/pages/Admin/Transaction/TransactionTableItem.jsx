import React from 'react';
import Moment from 'moment';

const TransactionTableItem = (props) => {
    const {transaction} = props;
    const formattedDate = Moment(transaction.created_at).format('DD/MM/YYYY');

    return (
        <tr className="border-b hover:bg-orange-100 transition duration-200 ease-in-out">
            <td><span className="block p-3 px-5">{transaction.price}</span></td>
            <td><span className="block p-3 px-5">{transaction.currency}</span></td>
            <td><span className="block p-3 px-5">{transaction.status}</span></td>
            <td><span className="block p-3 px-5">{transaction.token}</span></td>
            <td><span className="block p-3 px-5">{formattedDate}</span></td>
        </tr>
    );
};

export default TransactionTableItem;