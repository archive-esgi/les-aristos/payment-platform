import React, {useEffect, useState, useLayoutEffect} from 'react';
import TextField from '../../../components/TextField/TextField';
import UserTableItem from './UserTableItem';
import useUser from '../../../hooks/useUser';

const UserTable = () => {
    const {userSelectors, userActions} = useUser();
    const token = localStorage.getItem('token');
    const [filterUsers, setFilterUsers] = useState([]);
    const users = userSelectors.getUsers();

    useEffect(() => {
        userActions.fetchUsers(token);
        if (users) {
            setFilterUsers(users);
        }
    }, [])

    const renderUserList = () => {
        if (filterUsers.length > 0) {
            return filterUsers.map(user => {
                return <UserTableItem key={user.id} user={user} />;
            })
        }
        return (
            <tr><td className="text-center" colSpan={5}>Il n'y a pas d'utilisateur actuellement.</td></tr>
        );
    }

    function onSearch(event) {
        setFilterUsers(userSelectors.filterUsers(event.target.value));
    }

    return (
        <div className="ml-64 bg-gray-300">
            <div className="max-w-full w-full lg:max-w-full p-8 h-screen">
                <div className="overflow-x-auto bg-white rounded-lg p-4 flex flex-col justify-between leading-normal shadow-lg">
                    <p className="text-gray-900 font-bold text-xl mb-6 border-b">Liste des utilisateurs</p>
                    <div className="mb-2">
                        <label className="inline uppercase tracking-wide text-gray-700 text-xs font-bold mr-2">Rechercher : </label>
                        <TextField
                            className="mt-2 w-64 appearance-none inline bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-3 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            type="search"
                            name="search"
                            onChange={onSearch}
                            placeholder="Email"
                        />
                    </div>
                    <table className="w-full text-md bg-white shadow rounded mb-4">
                        <thead>
                            <tr className="bg-gray-200 border-b">
                                <th className="text-left p-3 px-5">E-mail</th>
                                <th className="text-left p-3 px-5">Date création</th>
                                <th className="text-left p-3 px-5">Etat</th>
                            </tr>
                        </thead>
                        <tbody>
                            {renderUserList()}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default UserTable;