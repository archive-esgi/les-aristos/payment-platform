import React from 'react';

const SignupConfirm = () => {
    return (
        <React.Fragment>
            <div className="flex justify-center w-full lg:max-w-full lg:flex">
                <div className="border border-gray-400 bg-white rounded-b p-4 mt-5 mx-5">
                    <div className="mb-8">
                        <div className="text-gray-900 font-bold text-xl mb-2">
                            Merci pour votre inscription !
                        </div>
                        <p className="text-gray-700 text-base">Votre compte est en attente de validation par les administrateurs de la plateforme. Un e-mail automatique vous a été envoyé avec plus d’informations.</p>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default SignupConfirm;