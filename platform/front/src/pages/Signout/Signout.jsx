import React, {useEffect} from 'react';
import useAuth from '../../hooks/useAuth';
import {useHistory, Redirect} from 'react-router-dom';


const Signout = () => {
    const {actions} = useAuth();
    const history = useHistory();

    useEffect(() => {
        actions.logout();
        history.go('/signin');
    })

    return (
        <React.Fragment>
            <h1>Disconnected</h1>
        </React.Fragment>
    )
}

export default Signout;