import React, {useEffect} from 'react';
import useTransaction from '../../hooks/useTransaction';
import TextField from '../../components/TextField/TextField';
import TransactionTableItem from './TransactionTableItem';
import useUser from '../../hooks/useUser';
import useMerchant from '../../hooks/useMerchant';
import useAuth from '../../hooks/useAuth';

const TransactionTable = () => {
    const {merchantSelectors, merchantActions} = useMerchant();
    const {actions, selectors} = useAuth();

    const token = localStorage.getItem('token');
    const merchantId = selectors.userInfo().merchantId;

    const merchantTransactions = merchantSelectors.getMerchant();

    useEffect(() => {
        merchantActions.fetchMerchant(token, merchantId);
    }, [])

    const renderTransactionTable = () => {
        if (merchantTransactions && merchantTransactions.Transactions.length > 0) {
            return merchantTransactions.Transactions.map(transaction => {
                return <TransactionTableItem key={transaction.id} transaction={transaction} />;
            })
        } else {
            return (
                <tr><td className="text-center" colSpan={5}>Il n'y a pas de transaction actuellement.</td></tr>
            );
        }
    }

    return (
        <div className="ml-64 bg-gray-300">
            <div className="max-w-full w-full lg:max-w-full p-8 h-screen">
                <div className="overflow-x-auto bg-white rounded-lg p-4 flex flex-col justify-between leading-normal shadow-lg">
                    <p className="text-gray-900 font-bold text-xl mb-6 border-b">Mes transactions</p>
                    <table className="w-full text-md bg-white shadow rounded mb-4">
                        <thead>
                            <tr className="bg-gray-200 border-b">
                                <th className="text-left p-3 px-5">Montant</th>
                                <th className="text-left p-3 px-5">Devise</th>
                                <th className="text-left p-3 px-5">Statut</th>
                                <th className="text-left p-3 px-5">Date de transaction</th>
                            </tr>
                        </thead>
                        <tbody>
                            {renderTransactionTable()}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default TransactionTable;