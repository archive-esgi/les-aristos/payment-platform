import {BASE_API} from '../../Constants';

export const FETCH_TRANSACTIONS = 'FETCH_TRANSACTIONS';
export const FETCH_TRANSACTION_BY_TOKEN = 'FETCH_TRANSACTION_BY_TOKEN';
export const UPDATE_TRANSACTION = 'UPDATE_TRANSACTION';
export const DELETE_TRANSACTION = 'DELETE_TRANSACTION';
export const PAYMENT_TRANSACTION_SUCCESS = 'PAYMENT_TRANSACTION_SUCCESS';
export const CANCEL_TRANSACTION = 'CANCEL_TRANSACTION';
export const PAYMENT_TRANSACTION_ERROR = 'PAYMENT_TRANSACTION_ERROR';

export const fetchTransactions = (token) => 
    fetch(`${BASE_API}/transactions`, {
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    }).then(res => res.json());


export const fetchTransactionByToken = (token) => 
fetch(`${BASE_API}/transactions/token/${token}`, {
    headers: {
        'Content-Type': 'application/json'
    }
}).then(res => res.json());

export const paymentTransaction = (param) => 
    fetch(`${BASE_API}/transactions/payment`, {
        method: 'POST',
        body: JSON.stringify(param),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(res => res);

export const cancelTransaction = (param) => 
    fetch(`${BASE_API}/transactions/cancel`, {
        method: 'POST',
        body: JSON.stringify(param),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(res => res);