import {BASE_API} from '../../Constants';

// action type
export const SET_AUTH = 'SET_AUTH';
export const SIGNUP_AUTH = 'SIGNUP_AUTH';
export const ERROR_AUTH = 'ERROR_AUTH';

export const login = (param) => 
    fetch(`${BASE_API}/users/login`, {
        method: 'POST',
        body: JSON.stringify(param),
        headers: {
            'Content-type': 'application/json'
        },
    })
    .then(res => res)
    .catch(err => console.error(err));


export const signup = param => 
    fetch(`${BASE_API}/users/signup`, {
        method: 'POST',
        body: JSON.stringify(param),
        headers: {
            'Content-type': 'application/json'
        },
    }).then(res => res.json());
