import {BASE_API} from '../../Constants';

export const FETCH_MERCHANTS = 'FETCH_MERCHANTS';
export const FETCH_MERCHANT = 'FETCH_MERCHANT';
export const UPDATE_MERCHANT = 'UPDATE_MERCHANT';
export const DELETE_MERCHANT = 'DELETE_MERCHANT';
export const SEND_KBIS_TO_RECEIVE_ID = 'SEND_KBIS_TO_RECEIVE_ID';
export const SEND_KBIS_TO_RECEIVE_ID_ERROR = 'SEND_KBIS_TO_RECEIVE_ID_ERROR';

export const fetchMerchants = (token) => 
    fetch(`${BASE_API}/merchants`, {
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    }).then(res => res.json());


export const fetchMerchant = (token, merchantId) => 
    fetch(`${BASE_API}/merchants/${merchantId}`, {
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    }).then(res => res.json());
;

export const updateMerchant = (token, userId, param) => 
    fetch(`${BASE_API}/merchants/${userId}`, {
        method: 'PUT',
        body: JSON.stringify(param),
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    }).then(res => res.json())


export const deleteMerchant = (token, userId) => 
    fetch(`${BASE_API}/merchants/${userId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    })
    .then(res => console.log(res.text()));

export const sendKbisToReceiveId = (kbis) => 
    fetch(`${BASE_API}/merchants/kbis`, {
        method: 'POST',
        body: kbis,
    }).then(res => res)
    .catch(err => console.error(err));
    