import {BASE_API} from '../../Constants';

export const FETCH_CURRENCIES = 'FETCH_CURRENCIES';

export const fetchCurrencies = () => 
    fetch(`${BASE_API}/currencies`, {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json());
