import {SET_AUTH, SIGNUP_AUTH, ERROR_AUTH} from '../actions/authAction';
import jwtDecode from 'jwt-decode';

export const authInitialState = {
    user: localStorage.getItem('token')
        ? jwtDecode(localStorage.getItem('token'))
        : null,
    token: localStorage.getItem('token'),
    errorMsg: '',
};

/**
 * action = { type: String, payload: any }
 */
export const authReducer = (state, action) => {
    switch (action.type) {
        case SET_AUTH:
            action.stopPropagation = true;
            return {
                ...state,
                token: action.payload.token,
                user: action.payload.user
            };
        case SIGNUP_AUTH:
            return {
                ...state,
                user: action.payload.data
            };
        case ERROR_AUTH: {
            return {
                ...state,
                errorMsg: action.payload.errorMsg
            }
        }
        default:
            return state;
    } 
};