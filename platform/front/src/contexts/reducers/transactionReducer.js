import {
    FETCH_TRANSACTIONS,
    FETCH_TRANSACTION_BY_TOKEN,
    UPDATE_TRANSACTION,
    DELETE_TRANSACTION,
    PAYMENT_TRANSACTION_SUCCESS,
    PAYMENT_TRANSACTION_ERROR,
    CANCEL_TRANSACTION
} from '../actions/transactionAction';

export const transactionInitialState = {
    transactions: [],
    transaction: null,
    confirmationUrl: '',
    cancelUrl: '',
    errorMsg: '',
};

export const transactionReducer = (state, action) => {
    switch (action.type) {
        case FETCH_TRANSACTIONS:
            return {
                ...state,
                transactions: action.payload,
            };
        case FETCH_TRANSACTION_BY_TOKEN:
            return {
                ...state,
                transaction: action.payload,
            }
        case PAYMENT_TRANSACTION_SUCCESS:
            return {
                ...state,
                confirmationUrl: action.payload.confirmationUrl,
            }
        case CANCEL_TRANSACTION:
            return {
                ...state,
                cancelUrl: action.payload.cancelUrl,
            }
        case PAYMENT_TRANSACTION_ERROR:
            return {
                ...state,
                errorMsg: action.payload.errorMsg
            }
        case UPDATE_TRANSACTION:
            const updatedTransaction = action.payload;

            const updatedTransactions = state.transactions.map(transaction => {
                if (transaction.id === updatedTransaction.id) {
                    return updatedTransaction;
                }
                return transaction;
            })
            return {
                ...state,
                transactions: updatedTransactions,
            }
        case DELETE_TRANSACTION:
            return {
                ...state,
                transactions: state.transactions.filter(
                    transaction => transaction.id !== action.payload
                )
            };
        default:
            return state;
    }
};
  