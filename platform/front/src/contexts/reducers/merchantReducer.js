import {
    FETCH_MERCHANTS,
    FETCH_MERCHANT,
    UPDATE_MERCHANT,
    DELETE_MERCHANT,
    SEND_KBIS_TO_RECEIVE_ID,
    SEND_KBIS_TO_RECEIVE_ID_ERROR
} from '../actions/merchantAction';

export const merchantInitialState = {
    merchants: [],
    merchant: null,
    successUpdateMsg: '',
    kbisId: '',
    kbisUploadError: ''
};

export const merchantReducer = (state, action) => {
    switch (action.type) {
        case FETCH_MERCHANTS:
            return {
                ...state,
                merchants: action.payload,
            };
        case FETCH_MERCHANT:
            return {
                ...state,
                merchant: action.payload,
            }
        case UPDATE_MERCHANT:
            const updatedMerchant = action.payload;

            const updatedMerchants = state.merchants.map(merchant => {
                if (merchant.id === updatedMerchant.id) {
                    return updatedMerchant;
                }
                return merchant;
            })
            return {
                ...state,
                merchants: updatedMerchants,
                successUpdateMsg: action.payload.successUpdateMsg
            }
        case DELETE_MERCHANT:
            return {
                ...state,
                merchants: state.merchants.filter(
                    merchant => merchant.id !== action.payload
                )
            };
        case SEND_KBIS_TO_RECEIVE_ID:
            return {
                ...state,
                kbisId: action.payload.kbisId
            };
        case SEND_KBIS_TO_RECEIVE_ID_ERROR:
            return {
                ...state,
                kbisUploadError: action.payload.kbisUploadError
            };
        default:
            return state;
    }
};
  