import {
    FETCH_USERS,
    FETCH_USER,
    UPDATE_USER,
    DELETE_USER
} from '../actions/userAction';

export const userInitialState = {
    users: [],
    user: null,
    successUpdateUserMsg: ''
};

export const userReducer = (state, action) => {
    switch (action.type) {
        case FETCH_USERS:
            return {
                ...state,
                users: action.payload,
            };
        case FETCH_USER:
            return {
                ...state,
                user: action.payload,
            }
        case UPDATE_USER:
            const updatedUser = action.payload;

            const updatedUsers = state.users.map(user => {
                if (user.id === updatedUser.id) {
                    return updatedUser;
                }
                return user;
            })
            return {
                ...state,
                users: updatedUsers,
                successUpdateUserMsg: action.payload.successUpdateUserMsg
            }
        case DELETE_USER:
            return {
                ...state,
                // user: action.payload
                users: state.users.filter(
                    user => user.id !== action.payload
                )
            };
        default:
            return state;
    }
};
  