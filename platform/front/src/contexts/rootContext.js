import React, {createContext, useState, useEffect, useReducer} from 'react';
import {authReducer, authInitialState} from './reducers/authReducer';
import {merchantReducer, merchantInitialState} from './reducers/merchantReducer';
import {transactionReducer, transactionInitialState} from './reducers/transactionReducer';
import {currencyReducer, currencyInitialState} from './reducers/currencyReducer';
import {userReducer, userInitialState} from './reducers/userReducer';

const RootContext = createContext(null);

const combineReducers = (reducers) => {
    return function rootReducer(state, action) {
        return Object.keys(reducers).reduce((acc, keyReducer) => {
            const newState = { ...acc };
            if (
            !action.stopPropagation &&
            (!action.audience || action.audience === keyReducer)
            ) {
                newState[keyReducer] = reducers[keyReducer](state[keyReducer], action);
            }
            return newState;
        }, state);
    };
};

const rootReducer = combineReducers({
    auth: authReducer,
    merchant: merchantReducer,
    transaction: transactionReducer,
    currency: currencyReducer,
    user: userReducer,
});

const initialState = {
    auth: authInitialState,
    merchant: merchantInitialState,
    transaction: transactionInitialState,
    currency: currencyInitialState,
    user: userInitialState
};

export const RootProvider = ({children}) => {
    const [state, dispatch] = useReducer(rootReducer, initialState);

    console.log(state)
    return (
        <RootContext.Provider value={{state, dispatch}}>
            {children}
        </RootContext.Provider>
    );
};

export default RootContext;