import {useContext, useState} from 'react';
import RootContext from '../contexts/rootContext';
import {
    fetchUsers as aFetchUsers,
    fetchUser as aFetchUser,
    updateUser as aUpdateUser,
    deleteUser as aDeleteUser,
    FETCH_USERS,
    FETCH_USER,
    UPDATE_USER,
    DELETE_USER,
} from '../contexts/actions/userAction';
import {useHistory} from 'react-router-dom';

const useUser = () => {
    const history = useHistory();
    const {
        state: {
            user: userState
        },
        dispatch,   
    } = useContext(RootContext);
    
    const userActions = {
        fetchUsers: (token) => {
            aFetchUsers(token)
                .then(users => {
                    dispatch({
                        type: FETCH_USERS,
                        payload: users
                    })
                });
        },
        fetchUser: (token, userId) => {
            aFetchUser(token, userId)
                .then(user => {
                    dispatch({
                        type: FETCH_USER,
                        payload: user
                    });
                });
        },
        updateUser: (token, userId, data) => {
            aUpdateUser(token, userId, data)
                .then(user => {
                    console.log(user);
                    dispatch({
                        type: UPDATE_USER,
                        payload: {
                            user,
                            successUpdateUserMsg: 'Les données ont été mise à jour.'
                        }
                    });
                });
        },
        deleteUser: (token, userId) => {
            aDeleteUser(token, userId)
                .then(userId => {
                    console.log(userId)
                    dispatch({
                        type: DELETE_USER,
                        payload: userId
                    })
                    history.push('/admin/users');
                })
        }
    };
    
    const userSelectors = {
        getUsers: () => userState.users || [],
        getSuccessUpdateUserMsg: () => userState.successUpdateUserMsg || '',
        getUser: () => userState.user || [],
        filterUsers: (email) => userState.users.filter(user => 
            user.email.toLowerCase().includes(email.toLowerCase())    
        ),
    };

    return {userSelectors, userActions};
}

export default useUser;