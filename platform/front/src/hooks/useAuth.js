import React, {useContext, useState, useEffect} from 'react';
import jwtDecode from 'jwt-decode';
import RootContext from '../contexts/rootContext';
import {login, signup, SET_AUTH, SIGNUP_AUTH, ERROR_AUTH} from '../contexts/actions/authAction';
import {useHistory, Redirect} from 'react-router-dom';
import { fetchMerchants, FETCH_MERCHANTS, fetchMerchant, FETCH_MERCHANT } from '../contexts/actions/merchantAction';
import { fetchUsers, FETCH_USERS } from '../contexts/actions/userAction';
import { fetchTransactions, FETCH_TRANSACTIONS } from '../contexts/actions/transactionAction';

const useAuth = () => {
    const history = useHistory();
    const {
        state: {auth: authState},
        dispatch,
    } = useContext(RootContext);

    const actions = {
        login: (data) => {
            login(data)
                .then(async (data) => {
                    if (data.status === 401) {
                        const dataJson = await data.json();
                        dispatch({
                            type: ERROR_AUTH,
                            payload: {
                                errorMsg: dataJson.message
                            },
                        });
                    }
                    else if (data.status === 200) {
                        const dataJson = await data.json();
                        const user = jwtDecode(dataJson.token);
                        localStorage.setItem('token', dataJson.token);
                        localStorage.setItem('merchantId', user.merchantId);
                        dispatch({
                            type: SET_AUTH,
                            payload: {
                                user,
                                token: dataJson.token,
                                merchantId: user.merchantId
                            },
                            audience: 'auth',
                        });
                        if (user.role === 0) {
                            history.push('/dashboard');
                            fetchMerchant(dataJson.token, user.merchantId).then(data => 
                                dispatch({
                                    type: FETCH_MERCHANT,
                                    payload: data
                                })
                            )
                        }
                        if (user.role === 1) {
                            history.push('/admin/dashboard');
                            fetchMerchants(dataJson.token).then(data => 
                                dispatch({
                                    type: FETCH_MERCHANTS,
                                    payload: data
                                })
                            )
                            fetchUsers(dataJson.token).then(data => 
                                dispatch({
                                    type: FETCH_USERS,
                                    payload: data
                                })    
                            )
                            fetchTransactions(dataJson.token).then(data => 
                                dispatch({
                                    type: FETCH_TRANSACTIONS,
                                    payload: data
                                })    
                            )
                        }
                        
                    }
                })
                .catch((err) => console.error(err))
        },
        signup: (data) => {
            signup(data)
                .then(data => {
                    dispatch({
                        type: SIGNUP_AUTH,
                        payload: {
                            data
                        },
                    });
                    history.push('/signup-confirmation');
                })
                .catch(err => console.error(err));
        },
        logout: () => {
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            history.push('/signin')
        },
    };
    
    const selectors = {
        isLogged: () => !!authState.token,
        errorMsg: () => authState.errorMsg || '',
        userInfo: () => authState.user || [],
        userToken: () => authState.token || ''
    };

    return {selectors, actions};
}

export default useAuth;