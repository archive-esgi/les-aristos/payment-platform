import {useState, useEffect} from 'react';

const useForm = (callback, validate) => {
    const [inputFormValues, setInputFormValues] = useState({});
    const [errors, setErrors] = useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);

    useEffect(() => {
        console.log(errors);

        if (Object.values(errors).every((val, i, arr) => val === arr[0])
            && isSubmitting) {
            callback();
        }
    }, [errors]);

    const handleSubmit = event => {
        if (event) event.preventDefault();
        setIsSubmitting(true);
        setErrors(validate(inputFormValues));
    }

    const handleChange = event => {
        const {name, value} = event.target;
        event.persist();
        setInputFormValues(inputFormValues => ({
            ...inputFormValues,
            [name]: value
        }));

        console.log(inputFormValues)
    }

     // const handleBlur = event => {
    //     const attribute = event.target.getAttribute('name');
    //     setInputValues(inputValues => ({
    //         ...inputValues,
    //         [attribute]: event.target.value.trim()
    //     }));
    // }

    return {handleSubmit, handleChange, inputFormValues, errors};
}

export default useForm;