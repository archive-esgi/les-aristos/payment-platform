import {useState, useEffect} from 'react';

const useLoading = () => {
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(false);
    }, []);

    const startLoading = () => {
        setIsLoading(true);
    }

    return {startLoading, isLoading, setIsLoading};
}

export default useLoading;