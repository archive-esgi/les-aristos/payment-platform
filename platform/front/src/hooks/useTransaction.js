import React, {useContext, useState} from 'react';
import RootContext from '../contexts/rootContext';
import {
    fetchTransactions as aFetchTransactions,
    fetchTransactionByToken as aFetchTransactionByToken,
    paymentTransaction as aPaymentTransaction,
    cancelTransaction as aCancelTransaction,
    FETCH_TRANSACTIONS,
    FETCH_TRANSACTION_BY_TOKEN,
    PAYMENT_TRANSACTION_SUCCESS,
    CANCEL_TRANSACTION,
    PAYMENT_TRANSACTION_ERROR
} from '../contexts/actions/transactionAction';
import {useHistory, Redirect} from 'react-router-dom';
const useTransaction = () => {
    const {
        state: {
            transaction: transactionState
        },
        dispatch,
    } = useContext(RootContext);
    const history = useHistory();

    const transactionActions = {
        fetchTransactions: (token) => {
            aFetchTransactions(token)
                .then(transactions => {
                    dispatch({
                        type: FETCH_TRANSACTIONS,
                        payload: transactions
                    })
                });
        },
        fetchTransactionByToken: (token, transactionId) => {
            aFetchTransactionByToken(token, transactionId)
                .then(transaction => {
                    dispatch({
                        type: FETCH_TRANSACTION_BY_TOKEN,
                        payload: transaction
                    });
                });
        },
        paymentTransaction: (data) => {
            aPaymentTransaction(data)
                .then(async data => {
                    if (data.status === 400) {
                        const dataJson = await data.json();

                        dispatch({
                            type: PAYMENT_TRANSACTION_ERROR,
                            payload: {
                                errorMsg: dataJson.message
                            }
                        })
                    }
                    else if (data.status === 200) {
                        const dataJson = await data.json();
                        dispatch({
                            type: PAYMENT_TRANSACTION_SUCCESS,
                            payload: {
                                confirmationUrl: dataJson.confirmationUrl
                            }
                        });
                        window.location.href = dataJson.confirmationUrl;
                    }
                });
        },
        cancelTransaction: (data) => {
            aCancelTransaction(data)
                .then(async data => {
                    if (data.status === 200) {
                        const dataJson = await data.json();
                        console.log(dataJson)
                        dispatch({
                            type: CANCEL_TRANSACTION,
                            payload: {
                                cancelUrl: dataJson.cancelUrl
                            }
                        });
                        window.location.href = dataJson.cancelUrl;
                    }
                });
        },
    };
    
    const transactionSelectors = {
        getTransactions: () => transactionState.transactions || [],
        getTransactionByToken: () => transactionState.transaction || '',
        errorMsg: () => transactionState.errorMsg || '',
        confirmationUrl: () => transactionState.confirmationUrl || '',
        cancelUrl: () => transactionState.cancelUrl || '',
        filterTransactions: (price) => transactionState.transactions.filter(transaction => 
            transaction.price.toString().includes(price)
        ),
    };

    return {transactionSelectors, transactionActions};
}

export default useTransaction;