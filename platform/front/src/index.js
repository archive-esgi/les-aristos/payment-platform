import React from 'react';
import ReactDOM from 'react-dom';
import Router from './Router';
import {BrowserRouter} from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import './assets/styles/main.css';
import { RootProvider } from './contexts/rootContext';

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <RootProvider>
                <Router />
            </RootProvider>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
