module.exports = app => {
    const transactionController = require('../controllers/TransactionController');
    const verifyJwt = require('../middlewares/verifyJwt');
    const verifyJwtAdmin = require('../middlewares/verifyJwtAdmin');
    const router = require('express').Router();

    router.post("/", transactionController.create);
    
    router.get("/", verifyJwtAdmin, transactionController.findAll);
    router.get("/:id", verifyJwt, transactionController.findOne);
    router.put("/:id", verifyJwt, transactionController.update);
    router.delete("/:id", verifyJwtAdmin, transactionController.delete);

    router.get("/token/:token", transactionController.findOneByToken);
    router.post("/payment", transactionController.payment);
    router.post("/cancel", transactionController.cancel);
    router.post("/refund", transactionController.refund);

    router.get("/kpi/transactions-counts/:days", verifyJwt, transactionController.transactionsCount);
    router.get("/kpi/money-earn/:days", verifyJwt, transactionController.moneyEarn);

    app.use('/api/transactions', router);
}
