module.exports = app => {
    const currencyController = require('../controllers/CurrencyController');
    const router = require('express').Router();

    router.get("/", currencyController.findAll);
    router.get("/:iso", currencyController.findOne);

    app.use('/api/currencies', router);
}
