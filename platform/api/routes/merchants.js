module.exports = app => {
    const merchantController = require('../controllers/MerchantController');
    const verifyJwt = require('../middlewares/verifyJwt');
    const verifyJwtAdmin = require('../middlewares/verifyJwtAdmin');
    const router = require('express').Router();

    router.get("/", verifyJwtAdmin, merchantController.findAll);
    router.get("/:id", verifyJwt, merchantController.findOne);
    router.put("/:id", verifyJwt, merchantController.update);
    router.delete("/:id", verifyJwtAdmin, merchantController.delete);

    router.post("/kbis", merchantController.kbisUpload);
    router.get("/kbis/:id", merchantController.getKbis);

    router.get("/credential-generator/:id", verifyJwt, merchantController.regenerateCredentials);

    app.use('/api/merchants', router);
}
