module.exports = app => {
    const userController = require('../controllers/UserController');
    const verifyJwt = require('../middlewares/verifyJwt');
    const verifyJwtAdmin = require('../middlewares/verifyJwtAdmin');
    const router = require('express').Router();

    router.post('/login', userController.login);
    router.post('/signup', userController.signup);

    router.get("/", verifyJwtAdmin, userController.findAll);
    router.get("/:id", verifyJwt, userController.findOne);
    router.put("/:id", verifyJwt, userController.update);
    router.delete("/:id", verifyJwtAdmin, userController.delete);

    app.use('/api/users', router);
}
