module.exports = app => {
    const KpiController = require('../controllers/KpiController');
    const verifyJwtAdmin = require('../middlewares/verifyJwtAdmin');
    const router = require('express').Router();
    
    router.get("/new-merchants/:days", verifyJwtAdmin, KpiController.newMerchant);
    router.get("/transactions-counts/:days", verifyJwtAdmin, KpiController.transactionsCount);

    app.use('/api/kpi', router);
}
