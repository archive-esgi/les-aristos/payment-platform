const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_DATABASE_URL, {
    dbName: process.env.MONGO_DATABASE_NAME,
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(res => console.log("MongoDB connected !!"))
.catch(err => console.log(`MongoDB connexion failed ${err}`));

module.exports = mongoose.connection;