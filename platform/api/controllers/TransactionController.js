'use strict';

const axios = require('axios');
const Transaction = dbpg.Transaction;
const TransactionSchema = require('../models/mongoose/TransactionSchema');
const MerchantSchema = require('../models/mongoose/MerchantSchema');
const CurrencySchema = require('../models/mongoose/CurrencySchema');
const UserSchema = require('../models/mongoose/UserSchema');
const transactions = require('../routes/transactions');

const prettifyErrors = (errors) => {
    return errors.reduce((acc, item) => {
        acc[item.path] = [...(acc[item.path] || []), item.message];
    
        return acc;
    }, {});
};

const isAllowedToAccess = async (id, user) => {
    if (user.role === 1) {
        return true;
    }

    let transaction = await TransactionSchema.findOne({ id })
        .then(transaction => (transaction ? res.json(transaction) : res.sendStatus(404)))
        .catch(() => res.sendStatus(500)
    );

    return  transaction.Merchant.id === user.id;
};

exports.create = async (req, res) => {
    let data = req.body;

    if (!data.currency) {
        return res.status(400).json({
            message: "currency property not found !"
        }).end();
    }

    let currencyObj = await CurrencySchema.findOne({ iso: data.currency })
        .then(currency => currency)
        .catch((error) => { 
            res.sendStatus(500).json(prettifyErrors(Object.values(error.errors))).end()
            return null;
    });

    if (!currencyObj) {
        return res.status(400).json({
            message: "This currency doesn't exist !"
        }).end();
    }

    data.Merchant = await MerchantSchema.findOne({ client_secret: data.client_secret })
        .then(merchant => merchant)
        .catch((error) => { 
            res.sendStatus(500).json(prettifyErrors(Object.values(error.errors))).end()
    });

    if (!data.Merchant) {
        return res.status(400).json({
            message: "Bad secret !"
        }).end();
    }

    if (!data.Merchant.User.active) {
        return res.status(400).json({
            message: "This merchant it not active"
        }).end();
    }

    data.currency = data.currency.toUpperCase();

    let transaction = await Transaction.create({
        price: data.price,
        currency: data.currency,
        token: Transaction.generateToken(),
        MerchantId: data.Merchant.id
    }, {include: [{model: dbpg.Merchant}]})
        .then(transaction => {
            transaction = transaction.toJSON();
            transaction.url = `http://localhost:3001/payment?t=${transaction.token}`;
            return res.status(201).json(transaction).end()
        })
        .catch(err => {
            if (err.name === 'ValidationError') {
                return res.status(400).json(prettifyErrors(Object.values(err.errors))).end();
            } else {
                console.log(err);
                return res.sendStatus(500).end();
            }
        }
    );
    
    return transaction;
}

exports.findAll = (req, res) => {
    TransactionSchema.find()
        .then(data => res.json(data))
        .catch(err => err.sendStatus(500)
    );
}

exports.findOne = async (req, res) => {
    if (await isAllowedToAccess(req.params.id, req.user)) {
        return res.sendStatus(404);
    }

    await TransactionSchema.findOne({ id: req.params.id })
        .then(transaction => (transaction ? res.json(transaction) : res.sendStatus(404)))
        .catch(() => res.sendStatus(500)
    );
};

exports.update = async (req, res) => {
    if (await isAllowedToAccess(req.params.id, req.user)) {
        return res.sendStatus(404);
    }

    await Transaction.update(req.body, {
        individualHooks: true,
        where: {id: req.params.id}
    })
    .then(([nbUpdate, transaction]) => {
        nbUpdate === 1 ? res.json(transaction) : res.sendStatus(404)
    })
    .catch(err => {
        if (err.name === 'ValidationError') {
            res.status(400).json(prettifyErrors(Object.values(err.errors)));
        } else {
            console.log(err);
            res.sendStatus(500);
        }
    });
};

exports.findOneByToken = (req, res) => {
    TransactionSchema.findOne({ token: req.params.token })
        .then(transaction => {
            if (transaction) {
                delete transaction.Merchant.siret;
                delete transaction.Merchant.phone;
                delete transaction.Merchant.kbis;
                delete transaction.Merchant.client_token;
                delete transaction.Merchant.client_secret;
                delete transaction.Merchant.url_confirmation;
                delete transaction.Merchant.url_cancel;
                delete transaction.Merchant.iban;
                delete transaction.Merchant.createdAt;
                delete transaction.Merchant.updatedAt;
                delete transaction.Merchant.deletedAt;
                return res.json(transaction);
            }else {
                return res.sendStatus(404).end();
            }
        })
        .catch(() => res.sendStatus(500)
    );
};

exports.delete = (req, res) => {
    Transaction.destroy({
        individualHooks: true,
        where: {id: req.params.id}
    })
    .then(transaction => (transaction ? res.sendStatus(204) : res.sendStatus(404)))
    .catch((err) => {
        console.log(err);
        res.sendStatus(500)
    })
};

exports.payment = async (req, res) => {
    if (!req.body.cardNumber || !req.body.cardOwner || !req.body.cardCrypt
         || !req.body.cardMonthEnd || !req.body.cardYearEnd || !req.body.transactionToken) {
        return res.status(400).end();
    }

    let transaction = await TransactionSchema.findOne({ token: req.body.transactionToken })
        .then(transaction => transaction)
        .catch(() => null);

    if (!transaction) {
        return res.status(400).json({
            message: "Unknown transaction"
        }).end();
    }

    if (transaction.status !== "created") {
        delete transaction.Merchant.siret;
        delete transaction.Merchant.phone;
        delete transaction.Merchant.kbis;
        delete transaction.Merchant.client_token;
        delete transaction.Merchant.client_secret;
        delete transaction.Merchant.url_confirmation;
        delete transaction.Merchant.url_cancel;
        delete transaction.Merchant.iban;
        delete transaction.Merchant.createdAt;
        delete transaction.Merchant.updatedAt;
        delete transaction.Merchant.deletedAt;
        return res.status(400).json({
            message: "Transaction already processed",
            transaction
        }).end();
    }

    let currency = await CurrencySchema.findOne({ iso: transaction.currency })
        .then(currency => currency)
        .catch(() => null);
    
    if (!currency) {
        return res.status(400).json({
            message: "Unknown currency"
        }).end();
    }

    let price = transaction.price !== 0 ? Math.floor(transaction.price / currency.value) : 0;

    let data = {
        cardNumber: req.body.cardNumber,
        cardOwner: req.body.cardOwner,
        cardCrypt: req.body.cardCrypt,
        cardMonthEnd: req.body.cardMonthEnd,
        cardYearEnd: req.body.cardYearEnd,
        ibanTarget: transaction.Merchant.iban,
        priceUSD: price
    };

    let request = await axios.post('http://psp:3000/payment', data)
        .then(async response => {
            if (response.status !== 200) {
                return res.status(400).end();
            }

            let transactionModel = await Transaction.update({
                status: "paid"
            }, {
                individualHooks: true,
                where: {id: transaction.id}
            })
                .then(([nbUpdate, transaction]) => transaction)
                .catch(err => {
                    if (err.name === 'ValidationError') {
                        return res.status(400).json(prettifyErrors(Object.values(err.errors))).end();
                    } else {
                        console.log(err);
                        return res.sendStatus(500).end();
                    }
                });

            transactionModel = await Transaction.findOne({where: {id: transaction.id}, include: {all: true}})
                .then(transaction => transaction)
                .catch(err => {
                    console.log(err);
                    return null;
                });

            let confirmationUrl = `${transactionModel.Merchant.url_confirmation}?ct=${transactionModel.Merchant.client_token}&t=${transactionModel.token}`;

            return res.status(200).json({
                confirmationUrl
            }).end();
        }).catch(error => {
            console.log(error);

            if (error.response.data && error.response.data.message) {
                console.log(error.response.data.message);
                return res.status(500).end();
            } else {
                return res.status(504).end();
            }
        });
    return request;
};

exports.cancel = async (req, res) => {
    if (!req.body.transactionToken) {
        return res.status(400).end();
    }

    let transaction = await TransactionSchema.findOne({ token: req.body.transactionToken })
        .then(transaction => transaction)
        .catch(() => null);

    if (!transaction) {
        return res.status(400).json({
            message: "Unknown transaction"
        }).end();
    }

    if (transaction.status !== "created") {
        delete transaction.Merchant.siret;
        delete transaction.Merchant.phone;
        delete transaction.Merchant.kbis;
        delete transaction.Merchant.client_token;
        delete transaction.Merchant.client_secret;
        delete transaction.Merchant.url_confirmation;
        delete transaction.Merchant.url_cancel;
        delete transaction.Merchant.iban;
        delete transaction.Merchant.createdAt;
        delete transaction.Merchant.updatedAt;
        delete transaction.Merchant.deletedAt;
        return res.status(400).json({
            message: "Transaction already processed",
            transaction
        }).end();
    }

    let transactionModel = await Transaction.update({
        status: "canceled"
    }, {
        individualHooks: true,
        where: {id: transaction.id}
    })
        .then(([nbUpdate, transaction]) => transaction)
        .catch(err => {
            if (err.name === 'ValidationError') {
                return res.status(400).json(prettifyErrors(Object.values(err.errors))).end();
            } else {
                console.log(err);
                return res.sendStatus(500).end();
            }
        });
    
    transactionModel = await Transaction.findOne({where: {id: transaction.id}, include: {all: true}})
        .then(transaction => transaction)
        .catch(err => {
            console.log(err);
            return null;
        });

    let cancelUrl = `${transactionModel.Merchant.url_cancel}?ct=${transactionModel.Merchant.client_token}&t=${transactionModel.token}`;

    return res.status(200).json({
        cancelUrl
    }).end();
};

exports.refund = async (req, res) => {
    if (!req.body.transactionToken && !req.body.client_secret) {
        return res.status(400).json({
            message: "Bad parameters"
        }).end();
    }

    let transaction = await TransactionSchema.findOne({ token: req.body.transactionToken })
        .then(transaction => transaction)
        .catch(() => null);

    if (!transaction) {
        return res.status(400).json({
            message: "Unknown transaction"
        }).end();
    }

    if (transaction.Merchant.client_secret !== req.body.client_secret) {
        return res.status(403).end();
    }

    if (transaction.status !== "paid") {
        delete transaction.Merchant.siret;
        delete transaction.Merchant.phone;
        delete transaction.Merchant.kbis;
        delete transaction.Merchant.client_token;
        delete transaction.Merchant.client_secret;
        delete transaction.Merchant.url_confirmation;
        delete transaction.Merchant.url_cancel;
        delete transaction.Merchant.iban;
        delete transaction.Merchant.createdAt;
        delete transaction.Merchant.updatedAt;
        delete transaction.Merchant.deletedAt;

        const message = transaction.status === "refunded" ? "Transaction already refunded" : "Transaction not paid";
        return res.status(400).json({
            message,
            transaction
        }).end();
    }

    let currency = await CurrencySchema.findOne({ iso: transaction.currency })
    .then(currency => currency)
    .catch(() => null);

    if (!currency) {
        return res.status(400).json({
            message: "Unknown currency"
        }).end();
    }

    let price = transaction.price !== 0 ? Math.floor(transaction.price / currency.value) : 0;
    let data = {
        ibanTarget: transaction.Merchant.iban,
        priceUSD: price
    };

    let request = await axios.post('http://psp:3000/refund', data)
        .then(async response => {
            if (response.status !== 200) {
                return res.status(400).json({
                    message: "Fail to connect the psp"
                }).end();
            }

            let transactionModel = await Transaction.update({
                status: "refunded"
            }, {
                individualHooks: true,
                where: {id: transaction.id}
            })
                .then(([nbUpdate, transaction]) => transaction)
                .catch(err => {
                    if (err.name === 'ValidationError') {
                        return res.status(400).json(prettifyErrors(Object.values(err.errors))).end();
                    } else {
                        console.log(err);
                        return res.sendStatus(500).end();
                    }
                });

            return res.status(200).json(transactionModel).end();
        }).catch(error => {
            console.log(error);

            if (error.response.data && error.response.data.message) {
                console.log(error.response.data.message);
                return res.status(500).end();
            } else {
                return res.status(504).end();
            }
        });
    return request;
};

exports.transactionsCount = async (req, res) => {
    if (!req.params.days) {
        return res.status(400).json({
            message: "No days given"
        }).end();
    }

    let user = await UserSchema.findOne({ id: req.user.id })
        .then(user => user)
        .catch(() => null);

    if (!user) {
        return res.status(400).json({
            message: "Unknown user"
        }).end();
    }

    var dateFrom = new Date();
    dateFrom.setDate(dateFrom.getDate() - req.params.days);

    TransactionSchema.find({
        createdAt: { $gte: dateFrom },
        "Merchant.id": user.Merchant.id
    })
        .then(data => {
            let created = 0;
            let paid = 0;
            let canceled = 0;
            let refunded = 0;

            if (data) {
                data.forEach(transaction => {
                    switch (transaction.status) {
                        case 'created':
                            created++;
                            break;
                        case 'paid':
                            paid++;
                            break;
                        case 'canceled':
                            canceled++;
                            break;
                        case 'refunded':
                            refunded++;
                            break;
                    }
                });
            }

            res.json({
                created,
                paid,
                canceled,
                refunded,
                total: data ? data.length : 0
            });
        })
        .catch(err => {
            console.log(err);
            err.sendStatus(500);
        });
}

exports.moneyEarn = async (req, res) => {
    if (!req.params.days) {
        return res.status(400).json({
            message: "No days given"
        }).end();
    }

    let user = await UserSchema.findOne({ id: req.user.id })
        .then(user => user)
        .catch(() => null);

    if (!user) {
        return res.status(400).json({
            message: "Unknown user"
        }).end();
    }

    var dateFrom = new Date();
    dateFrom.setDate(dateFrom.getDate() - req.params.days);

    TransactionSchema.find({
        createdAt: { $gte: dateFrom },
        "Merchant.id": user.Merchant.id
    })
        .then(data => {
            let amount = 0; 

            if (data) {
                data.forEach(transaction => {
                    if (transaction.status === "paid") {
                        amount += transaction.price;
                    }
                });
            }

            res.json({
                amount
            });
        })
        .catch(err => {
            console.log(err);
            err.sendStatus(500);
        });
}
