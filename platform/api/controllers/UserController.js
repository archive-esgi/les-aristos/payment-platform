const UserSchema = require('../models/mongoose/UserSchema');
const MerchantSchema = require('../models/mongoose/MerchantSchema');
const nodemailer = require("nodemailer");
const createToken = require('../lib/jwt').createToken;
const argon2 = require('argon2');

const prettifyErrors = (errors) => {
    return errors.reduce((acc, item) => {
        acc[item.path] = [...(acc[item.path] || []), item.message];
    
        return acc;
    }, {});
};

exports.login = async (req, res) => {
    const {email, password} = req.body;
    const buffedPassword = new Buffer.from(password);

    let user = await UserSchema.findOne({email})
        .then(user => user)
        .catch(error => {
            console.log(error);
        });

    if (!user) {
        res.status(401).send({
            message: 'Les informations de connexion sont incorrectes'
        });
    } else {
        argon2.verify(user.password, buffedPassword).then(isValid => {
            if (isValid) {
                if (user.active) {
                    createToken({
                        id: user.id,
                        email,
                        role: user.role,
                        merchantId: user.Merchant.id
                    })
                    .then(token => res.json({token}))
                    .catch(() => res.sendStatus(500));
                } else {
                    res.status(401).json({
                        message: 'Votre compte est en attente de validation, nous vous enverrons un email pour vous confirmer son activation'
                    });
                }
            } else {
                res.status(401).json({
                    message: 'Les informations de connexion sont incorrectes'
                });
            }
        });
    }
}

exports.signup = async (req, res) => {
    let user = UserSchema.findOne({ email: req.body.email })
        .then(user => user)
        .catch(() => res.sendStatus(500))

    if (!user) {
        return res.status(400).json({
            message: "Un utilisateur avec cette adresse email existe déjà."
        });
    }

    let merchant = MerchantSchema.findOne({ siret: req.body.siret })
        .then(user => user)
        .catch(() => res.sendStatus(500))

    if (!merchant) {
        return res.status(400).json({
            message: "Un marchand avec ce siret existe déjà."
        });
    }

    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: process.env.GMAIL_MAIL,
          pass: process.env.GMAIL_PASS
        }
    });

    const mailOptions = {
        from: 'platform.test.esgi@gmail.com',
        to: req.body.email,
        subject: 'Validation de votre compte en attente',
        html: `Bonjour ${req.body.social_reason},<br/>Votre compte est actuellement en attente de validation par un administrateur, merci de patienter.`
    };

    dbpg.UserAccount.create({
        email: req.body.email,
        password: req.body.password,
        Merchant: {
            social_reason: req.body.social_reason,
            siret: req.body.siret,
            phone: req.body.phone,
            kbis: req.body.kbis,
            client_token: dbpg.Merchant.generateCredential(),
            client_secret: dbpg.Merchant.generateCredential(),
            url_confirmation: req.body.url_confirmation,
            url_cancel: req.body.url_cancel,
            iban: req.body.iban,
            currency: req.body.currency
        }
    }, {
        include:[dbpg.Merchant]
      })
        .then(user => {
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                  console.log(error);
                } else {
                  console.log('Email sent: ' + info.response);
                }
            });

            return res.status(201).json(user)
        })
        .catch(err => {
            if (err.name === 'ValidationError') {
                res.status(400).json(prettifyErrors(Object.values(err.errors)));
            } else {
                console.log(err);
                res.sendStatus(500);
            }
        }
    );
}

exports.findAll = (req, res) => {
    UserSchema.find()
        .then(data => res.json(data))
        .catch(err => err.sendStatus(500)
    );
}

exports.findOne = (req, res) => {
    UserSchema.findOne({ id: req.params.id })
        .then(user => (user ? res.json(user) : res.sendStatus(404)))
        .catch(() => res.sendStatus(500))
};

exports.update = (req, res) => {
    dbpg.UserAccount.update(req.body, {
        individualHooks: true,
        where: {id: req.params.id}
    })
    .then(([nbUpdate, user]) => {
        nbUpdate === 1 ? res.json(user) : res.sendStatus(404)
    })
    .catch(err => {
        if (err.name === 'ValidationError') {
            res.status(400).json(prettifyErrors(Object.values(err.errors)));
        } else {
            console.log(err);
            res.sendStatus(500);
        }
    });
};

exports.delete = (req, res) => {
    dbpg.UserAccount.destroy({
        individualHooks: true,
        where: {id: req.params.id}
    })
    .then(user => (user ? res.sendStatus(204) : res.sendStatus(404)))
    .catch((err) => {
        console.log(err);
        res.sendStatus(500);
    })
};