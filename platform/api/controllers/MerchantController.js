const fs = require("fs");
const path = require('path');
const Merchant = dbpg.Merchant;
const MerchantSchema = require('../models/mongoose/MerchantSchema');

const prettifyErrors = (errors) => {
    return errors.reduce((acc, item) => {
        acc[item.path] = [...(acc[item.path] || []), item.message];
    
        return acc;
    }, {});
};

exports.create = async (req, res) => {
    Merchant.create(req.body)
        .then(merchant => res.status(201).json(merchant))
        .catch(err => {
            if (err.name === 'ValidationError') {
                res.status(400).json(prettifyErrors(Object.values(err.errors)));
            } else {
                console.log(err);
                res.sendStatus(500);
            }
        }
    );
}

exports.findAll = (req, res) => {
    MerchantSchema.find()
        .then(data => res.json(data))
        .catch(err => err.sendStatus(500)
    );
}

exports.findOne = (req, res) => {
    MerchantSchema.findOne({ id: req.params.id })
        .then(merchant => (merchant ? res.json(merchant) : res.sendStatus(404)))
        .catch(() => res.sendStatus(500)
    )
};

exports.update = (req, res) => {
    Merchant.update(req.body, {
        individualHooks: true,
        where: {id: req.params.id}
    })
    .then(([nbUpdate, merchant]) => {
        nbUpdate === 1 ? res.json(merchant) : res.sendStatus(404)
    })
    .catch(err => {
        if (err.name === 'ValidationError') {
            res.status(400).json(prettifyErrors(Object.values(err.errors)));
        } else {
            console.log(err);
            res.sendStatus(500);
        }
    });
};

exports.delete = (req, res) => {
    Merchant.destroy({
        individualHooks: true,
        where: {id: req.params.id}
    })
    .then(merchant => (merchant ? res.sendStatus(204) : res.sendStatus(404)))
    .catch((err) => {
        console.log(err);
        res.sendStatus(500)
    })
};


exports.kbisUpload = (req, res) => {
    if (!req.files && !req.files.hasOwnProperty(kbis)) {
        return res.status(400).json({
            message: "No kbis finded"
        }).end();
    }

    if (req.files.kbis.mimetype !== "application/pdf") {
        return res.status(400).json({
            message: "Please upload a PDF file"
        }).end();
    }

    let kbis = req.files.kbis;
    let id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

    kbis.mv(`${__dirname}/../docs/kbis/${id}.pdf`, (err) => {
        if (err) {
            console.log(err);
            return res.status(500).end();
        }

        return res.status(201).json({
            id
        }).end();
    });
};

exports.getKbis = (req, res) => {
    if (!req.params.id) {
        return res.status(400).end();
    }

    let pathFile = path.resolve(`${__dirname}/../docs/kbis/${req.params.id}.pdf`);

    try {
        if (fs.existsSync(pathFile)) {
            res.sendFile(pathFile);
        } else {
            console.log(pathFile);
            return res.status(404).end();
        }
    } catch(err) {
        console.log(err);
        return res.status(500).end();
    }
};

exports.regenerateCredentials = (req, res) => {
    let client_token = Merchant.generateCredential();
    let client_secret = Merchant.generateCredential();

    Merchant.update({ client_token, client_secret }, {
        where: {id: req.params.id}
    })
    .then(([nbUpdate, merchant]) => {
        nbUpdate === 1 ? res.json(merchant) : res.sendStatus(404)
    })
    .catch(err => {
        if (err.name === 'ValidationError') {
            res.status(400).json(prettifyErrors(Object.values(err.errors)));
        } else {
            res.sendStatus(500);
        }
    });
};