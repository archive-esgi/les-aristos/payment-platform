const TransactionSchema = require('../models/mongoose/TransactionSchema');
const MerchantSchema = require('../models/mongoose/MerchantSchema');
const { create } = require('../models/mongoose/TransactionSchema');

const prettifyErrors = (errors) => {
    return errors.reduce((acc, item) => {
        acc[item.path] = [...(acc[item.path] || []), item.message];
    
        return acc;
    }, {});
};

exports.newMerchant = (req, res) => {
    if (!req.params.days) {
        return res.status(400).json({
            message: "No days given"
        }).end();
    }

    var dateFrom = new Date();
    dateFrom.setDate(dateFrom.getDate() - req.params.days);

    console.log(dateFrom);

    MerchantSchema.countDocuments({ createdAt: { $gte: dateFrom } })
        .then(data => {
            res.json({
                count: data
            });
        })
        .catch(err => {
            console.log(err);
            err.sendStatus(500);
        });
}

exports.transactionsCount = (req, res) => {
    if (!req.params.days) {
        return res.status(400).json({
            message: "No days given"
        }).end();
    }

    var dateFrom = new Date();
    dateFrom.setDate(dateFrom.getDate() - req.params.days);

    TransactionSchema.find({ createdAt: { $gte: dateFrom } })
        .then(data => {
            let created = 0;
            let paid = 0;
            let canceled = 0;
            let refunded = 0;

            if (data) {
                data.forEach(transaction => {
                    switch (transaction.status) {
                        case 'created':
                            created++;
                            break;
                        case 'paid':
                            paid++;
                            break;
                        case 'canceled':
                            canceled++;
                            break;
                        case 'refunded':
                            refunded++;
                            break;
                    }
                });
            }

            res.json({
                created,
                paid,
                canceled,
                refunded,
                total: data ? data.length : 0
            });
        })
        .catch(err => {
            console.log(err);
            err.sendStatus(500);
        });
}
