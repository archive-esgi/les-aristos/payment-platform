const Scrapper = require('./lib/scrapper');
const Currency = require('./models/mongoose/CurrencySchema');

exports.updateCurrencies = () => {
    const url = 'http://api.currencylayer.com/live?access_key=87ee71538c4d4dbf450d548c1b224468';
    const requestOptions = {};

    const scrap = Scrapper(
        url, requestOptions,
        data => {
            let dataFormated = [];
            for (const [key, value] of Object.entries(data.quotes)) {
                dataFormated.push({
                    iso: key.substring(3),
                    value
                });
            }
            return dataFormated;
        },
        data => {
            data.forEach(async currency => {
                await Currency.deleteOne({ iso: currency.iso });
                await (new Currency(currency)).save();
            });
        }
    );
      
    scrap.end();
};