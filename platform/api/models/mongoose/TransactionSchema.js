const mongoose = require("mongoose");
const connect = require("../../lib/mongo");

const TransactionSchema = new mongoose.Schema({
    id: Number,
    status: String,
    price: Number,
    currency: String,
    token: String,
    createdAt: Date,
    updatedAt: Date,
    Merchant: Object
});

const Transaction = connect.model("Transaction", TransactionSchema);

module.exports = Transaction;