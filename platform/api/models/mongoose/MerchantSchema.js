const mongoose = require("mongoose");
const connect = require("../../lib/mongo");

const MerchantSchema = new mongoose.Schema({
    id: Number,
    social_reason: String,
    siret: String,
    phone: String,
    kbis: String,
    client_token: String,
    client_secret: String,
    url_confirmation: String,
    url_cancel: String,
    iban: String,
    currency: String,
    createdAt: Date,
    updatedAt: Date,
    User: Object,
    Transactions: Array
});

const Merchant = connect.model("Merchant", MerchantSchema);

module.exports = Merchant;