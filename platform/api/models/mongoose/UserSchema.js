const mongoose = require("mongoose");
const connect = require("../../lib/mongo");

const UserSchema = new mongoose.Schema({
    id: Number,
    email: String,
    role: Number,
    password: String,
    active: Boolean,
    createdAt: Date,
    updatedAt: Date,
    Merchant: Object
});

const User = connect.model("User", UserSchema);

module.exports = User;