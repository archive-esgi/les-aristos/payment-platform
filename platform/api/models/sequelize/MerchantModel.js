'use strict';

const merchantDenormalisation = require('./denormalization/merchant');
const userDenormalisation = require('./denormalization/user');
const transactionDenormalisation = require('./denormalization/transaction');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Merchant extends Model {
        static associate(models) {
            Merchant.hasOne(models.UserAccount, {foreignKey: 'MerchantId', as: 'User'});
            Merchant.hasMany(models.Transaction, {as: 'Transactions'});
        }
    }

    Merchant.init(
        {
            social_reason: {
                type: DataTypes.STRING(60),
                allowNull: false
            },
            siret: {
                type: DataTypes.STRING(14),
                unique: true,
                allowNull: false,
            },
            phone: {
                type: DataTypes.STRING(25),
                allowNull: false,
            },
            kbis: {
                type: DataTypes.STRING(255),
                allowNull: false
            },
            client_token: {
                type: DataTypes.STRING(64),
                unique: true,
                allowNull: false
            },
            client_secret: {
                type: DataTypes.STRING(64),
                unique: true,
                allowNull: false
            },
            url_confirmation: {
                type: DataTypes.STRING,
                allowNull: false
            },
            url_cancel: {
                type: DataTypes.STRING,
                allowNull: false
            },
            iban: {
                type: DataTypes.STRING(34),
                allowNull: false,
                validate: {
                    len: [14, 34]
                }
            },
            currency: {
                type: DataTypes.STRING(3),
                allowNull: false,
                validate: {
                    isUppercase: true
                }
            }
        },
        {
            sequelize,
            modelName: 'Merchant',
            timestamps: true,
            paranoid: true
        }
    );

    Merchant.generateCredential = () => {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    };
    
    Merchant.addHook("afterCreate", async (merchant) => {
        await merchantDenormalisation.denormalize(merchant, "create");
    });
    Merchant.addHook("afterUpdate", async (merchant) => {
        await Merchant.findOne({where: {id: merchant.id}, include: {all: true}})
            .then(async merchantFinded => {
                await merchantDenormalisation.denormalize(merchantFinded, "update");
                await userDenormalisation.denormalize(merchantFinded.User, "update");
                merchantFinded.Transactions.forEach(async transaction => {
                    await transactionDenormalisation.denormalize(transaction, "update");
                });
            })
            .catch(err => {
                console.log(err);
            });
    });
    Merchant.addHook("afterDestroy", async (merchant) => {
        await Merchant.findOne({where: {id: merchant.id}, include: {all: true}})
            .then(async merchantFinded => {
                await merchantDenormalisation.denormalize(merchantFinded, "delete");
                await userDenormalisation.denormalize(merchantFinded.User, "delete");
                merchantFinded.Transactions.forEach(async transaction => {
                    await transactionDenormalisation.denormalize(transaction, "delete");
                });
            })
            .catch(err => {
                console.log(err);
            });
    });

    return Merchant;
};
