'use strict';

const userDenormalisation = require('./denormalization/user');
const merchantDenormalisation = require('./denormalization/merchant');
const argon2 = require('argon2');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        static associate(models) {
            User.belongsTo(models.Merchant);
        }
    }

    User.init(
        {
            email: {
                type: DataTypes.STRING,
                unique: true,
                allowNull: false,
                validate: {
                    isEmail: {msg: 'Invalid email format'}
                }
            },
            role: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0
            },
            password: {
                type: DataTypes.STRING(128),
                allowNull: false
            },
            active: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            }
        },
        {
            sequelize,
            modelName: 'UserAccount',
            timestamps: true,
            paranoid: true
        }
    );

    User.addHook('beforeCreate', async (user) => {
        user.password = await argon2.hash(user.password);
    });
    
    User.addHook('beforeUpdate', async (user) => {
        if (!user.password.startsWith('$argon')) {
            user.password = await argon2.hash(user.password);
        }
    });
    
    User.addHook("afterCreate", async (user) => {
        await userDenormalisation.denormalize(user, "create");
        await merchantDenormalisation.denormalize(user.Merchant);
    });
    User.addHook("afterUpdate", async (user) => {
        await User.findOne({where: {id: user.id}, include: {all: true}})
            .then(async userFinded => { 
                await userDenormalisation.denormalize(userFinded, "update");
                await merchantDenormalisation.denormalize(userFinded.Merchant, "update");
            })
            .catch(err => {
                console.log(err);
            });
    });
    User.addHook("afterDestroy", async (user) => {
        await User.findOne({where: {id: user.id}, include: {all: true}})
            .then(async userFinded => { 
                await userDenormalisation.denormalize(userFinded, "delete");
                await merchantDenormalisation.denormalize(userFinded.Merchant, "delete");
            })
            .catch(err => {
                console.log(err);
            });
    });

    return User;
};
